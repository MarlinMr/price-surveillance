#!/usr/bin/python3.7
import json
import discord
from discord_webhook import DiscordWebhook, DiscordEmbed
import yfinance as yf
import os

dirname = os.path.dirname(__file__)
config_file = os.path.join(dirname, "./src/stonk-bot.conf")
data_file = os.path.join(dirname, "./src/stonk-bot.json")


with open(config_file, 'r', encoding='utf8') as f:
    data = json.load(f)
    webhook_url = data['webhook']

with open(data_file, 'r', encoding='utf8') as f:
    data = json.load(f)

webhook = DiscordWebhook(url=webhook_url, avatar_url='https://static.wikia.nocookie.net/disney/images/9/95/Profile_-_Zazu.jpeg', username='Zazu')
embed = DiscordEmbed(title="Morning report", description=f"Endringer fra i går")

#embed.set_thumbnail(url=ticker['logo_url'])

for stonk in data['stonks']:
    ticker = yf.Ticker(stonk).info
    diff = ticker['ask'] - ticker['previousClose']
    diff2 = 100*diff/ticker['ask']
    embed.add_embed_field(name=ticker['shortName'], value=f"{round(diff2,2)}% ({round(diff, 2)} {ticker['currency']})", inline=True)
    #embed.add_embed_field(name="Fra", value=f"{ticker['currency']} {data['stonks'][stonk]['last']}", inline=True)
    #embed.add_embed_field(name="Til", value=f"{ticker['currency']} {ticker['ask']}", inline=True)
    #content=f"{ticker['shortName']} har endra seg {100*diff}% siste 15 minuttene! Fra {ticker['currency']} {data['stonks'][stonk]['last']} til {ticker['currency']} {ticker['ask']}")
webhook.add_embed(embed)
response = webhook.execute()

with open(data_file, 'w', encoding='utf8') as f:
    data = json.dump(data, f)
